import React, { Component } from "react";
import { phoneArr } from "../../../data/data";
import SanPham from "./SanPham";

export default class DanhSachSP extends Component {
  state = {
    sanPhamChiTiet: phoneArr[0],
  };
  renderSanPham = () => {
    return phoneArr.map((phone, index) => {
      return (
        <SanPham phone={phone} key={index} viewDetail={this.handleViewDetail} />
      );
    });
  };

  renderChiTiet = (spChiTiet) => {
    return (
      <div>
        <h4>{spChiTiet.tenSP}</h4>
        <p>{spChiTiet.giaBan}</p>
      </div>
    );
  };

  handleViewDetail = (phone) => {
    this.setState({
      sanPhamChiTiet: phone,
    });
  };
  render() {
    return (
      <div className="container">
        <div className="row">{this.renderSanPham()}</div>
        <div className="row">
          {/* {this.renderChiTiet(this.state.sanPhamChiTiet)} */}
          <div>
            <h4>{this.state.sanPhamChiTiet.tenSP}</h4>
            <p>{this.state.sanPhamChiTiet.giaBan}</p>
          </div>
        </div>
      </div>
    );
  }
}
