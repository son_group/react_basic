import React, { Component } from "react";

export default class BaiTapState extends Component {
  state = {
    imgSrc: "../img/redcar.jpg",
  };

  handleChangeColor = (color) => {
    let imgsrc = "../img/redcar.jpg";
    switch (color) {
      case "blue":
        imgsrc = "../img/bluecar.webp";
        break;
      case "black":
        imgsrc = "../img/blackcar.webp";
        break;

      default:
        break;
    }
    this.setState({
      imgSrc: imgsrc,
    });
  };

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-6">
            <img src={this.state.imgSrc} style={{ heigh: 300, width: 300 }} />
          </div>
          <div className="col-6">
            <button
              onClick={() => {
                this.handleChangeColor("red");
              }}
              className="btn btn-danger mr-5"
            >
              red car
            </button>
            <button
              onClick={() => {
                this.handleChangeColor("blue");
              }}
              className="btn btn-secondary mr-5"
            >
              gray car
            </button>
            <button
              onClick={() => {
                this.handleChangeColor("black");
              }}
              className="btn btn-dark"
            >
              black car
            </button>
          </div>
        </div>
      </div>
    );
  }
}
