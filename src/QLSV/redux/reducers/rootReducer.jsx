import { combineReducers } from "redux";
import { EmployeeRequestReducer } from "./EmployeeRequestReducer";

export const rootReducer = combineReducers({
  EmployeeRequestReducer: EmployeeRequestReducer,
});
