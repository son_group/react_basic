import { Tag } from "antd";

export const headColumns = [
  {
    title: "Mã nhân viên",
    dataIndex: "employeeID",
    width: "25%",
    editable: false,
  },
  {
    title: "Loại yêu cầu",
    dataIndex: "requestType",
    width: "15%",
    editable: true,
  },
  {
    title: "Lý do",
    dataIndex: "requestReason",
    width: "40%",
    editable: true,
  },
  {
    title: "Ngày xin",
    dataIndex: "requestDate",
    width: "10%",
    editable: true,
  },
  {
    title: "Thời gian",
    dataIndex: "requestTime",
    width: "10%",
    editable: true,
  },
];
