import React, { Component } from "react";

export default class DemoIf extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLogin: false,
      userName: "",
    };
  }

  login = () => {
    this.setState({
      isLogin: true,
      userName: "Friday",
    });
  };

  logout = () => {
    this.setState({
      isLogin: false,
      userName: "",
    });
  };

  render() {
    return (
      <div className="container">
        {this.state.isLogin === true ? (
          <div>
            Hello {this.state.userName}
            <button onClick={this.logout}>Logout</button>
          </div>
        ) : (
          <button onClick={this.login}>Login</button>
        )}
      </div>
    );
  }
}
