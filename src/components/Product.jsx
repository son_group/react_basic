import React, { Component } from "react";

export default class Product extends Component {
  render() {
    return (
      <div className="container">
        <div className="card text-white bg-secondary">
          <img
            className="card-img-top border border-1"
            style={{ height: 50, width: 50 }}
            src="./logo512.png"
            alt="Anh logo"
          />
          <div className="card-body">
            <h4 className="card-title">Title</h4>
            <p className="card-text">Text</p>
          </div>
        </div>
      </div>
    );
  }
}
