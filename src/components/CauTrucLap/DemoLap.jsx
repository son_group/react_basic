import React, { Component } from "react";

export default class DemoLap extends Component {
  state = {
    mangSanPham: [
      { maSP: 1, tenSP: "iPhone", giaSP: 1000 },
      { maSP: 2, tenSP: "iPhone X", giaSP: 1200 },
      { maSP: 3, tenSP: "iPhone XS", giaSP: 1800 },
    ],
  };

  renderSanPham = () => {
    return this.state.mangSanPham.map((sanPham, index) => {
      return (
        <tr key={index}>
          <td>{sanPham.maSP}</td>
          <td>{sanPham.tenSP}</td>
          <td>{sanPham.giaSP}</td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div className="container">
        <h3>Danh sach san pham</h3>
        <table className="table">
          <thead>
            <tr>
              <th>ma sp</th>
              <th>ten sp</th>
              <th>gia sp</th>
            </tr>
          </thead>
          <tbody>{this.renderSanPham()}</tbody>
        </table>
      </div>
    );
  }
}
