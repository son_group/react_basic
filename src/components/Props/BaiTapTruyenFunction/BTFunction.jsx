import React, { Component } from "react";
import DanhSachSP from "./DanhSachSP";
import { phoneArr } from "../../../data/data";

export default class BTFunction extends Component {
  render() {
    return (
      <div className="text-center">
        <h3>Danh sách sản phẩm</h3>
        <DanhSachSP phoneList={phoneArr} />
      </div>
    );
  }
}
