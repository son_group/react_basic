import React, { Component } from "react";
import { phimArr } from "./data";

export default class ListFilms extends Component {
  state = {
    phimArr: phimArr,
  };

  renderListFilm = () => {
    return this.state.phimArr.map((phim, index) => {
      return (
        <div
          key={index}
          className="card col-3 m-1 border border-0"
          style={{ width: "15rem" }}
        >
          <img
            className="card-img-top"
            src={phim.hinhAnh}
            alt="Card cap"
            style={{ height: "15rem" }}
          />
          <div className="card-body">
            <h5 className="card-title">{phim.tenPhim}</h5>
            <p className="card-text">
              {phim.moTa.length < 100 ? phim.moTa : phim.moTa.slice(0, 100)}
            </p>
            <button className="btn btn-primary">Xem chi tiết</button>
          </div>
        </div>
      );
    });
  };
  render() {
    return (
      <div className="container">
        <div className="row">{this.renderListFilm()}</div>
      </div>
    );
  }
}
