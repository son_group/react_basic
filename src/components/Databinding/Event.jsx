import React, { Component } from "react";

export default class Event extends Component {
  handleShowMessage = (message) => {
    alert(message);
  };
  render() {
    let message = "Hom nay la ngay thu 6";
    return (
      <div className="container justify-item-center">
        <button
          onClick={() => {
            this.handleShowMessage(message);
          }}
          className="btn btn-secondary"
        >
          show message
        </button>
      </div>
    );
  }
}
