import React, { Component } from "react";

export default class SinhVien extends Component {
  hoTen = "Nguyen Ngoc Anh";
  lop = "Lop 1C";
  renderThongTinSV = () => {
    let truong = "THCS Thuan Thanh";
    return (
      <ul>
        <li>{this.hoTen}</li>
        <li>{this.lop}</li>
        <li>{truong}</li>
      </ul>
    );
  };
  render() {
    return <div className="container">{this.renderThongTinSV()}</div>;
  }
}
