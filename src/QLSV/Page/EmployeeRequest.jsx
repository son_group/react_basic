import React, { Component } from "react";
import ListReq from "./AntTable";
import RequestForm from "./RequestForm";
import RequestList from "./RequestList";
import RequestListU from "./RequestListU";

export default class EmployeeRequest extends Component {
  render() {
    return (
      <div className="container">
        <RequestForm />
        <RequestList />
      </div>
    );
  }
}
