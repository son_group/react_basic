import React, { Component } from "react";
import { connect } from "react-redux";

class RequestList extends Component {
  renderRequest = () => {
    const { requestList } = this.props;
    return requestList.map((request, index) => {
      return (
        <tr key={index}>
          <td>{request.employeeID}</td>
          <td>{request.requestType}</td>
          <td>{request.requestReason}</td>
          <td>{request.requestDate}</td>
          <td>{request.requestTime}</td>
          <td>{request.requestStatus}</td>
          <td>
            <div>
              <button type="button" className="btn ">
                <i className="fa fa-trash" />
              </button>
              <button type="button" className="btn ">
                <i className="fa fa-pen-square" />
              </button>
            </div>
          </td>
        </tr>
      );
    });
  };

  render() {
    return (
      <div className="container py-5">
        <table className="table">
          <thead>
            <tr>
              <th>Mã nhân viên</th>
              <th>Loại yêu cầu</th>
              <th>Lý do</th>
              <th>Ngày xin</th>
              <th>Thời gian</th>
              <th>Trạng thái</th>
              <th>Cập nhật</th>
            </tr>
          </thead>
          <tbody>{this.renderRequest()}</tbody>
        </table>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    requestList: state.EmployeeRequestReducer.requestList,
  };
};

export default connect(mapStateToProps, null)(RequestList);
