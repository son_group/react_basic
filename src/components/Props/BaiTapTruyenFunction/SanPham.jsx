import React, { Component } from "react";

export default class SanPham extends Component {
  render() {
    let { phone, viewDetail } = this.props;
    // let viewDetail = this.props.viewDetail;
    return (
      <div className="card mr-3" style={{ width: "18rem" }} key={phone.maSP}>
        <img
          className="card-img-top"
          src={phone.hinhAnh}
          alt="Card image cap"
        />
        <div className="card-body">
          <h5 className="card-title">{phone.tenSP}</h5>
          <p className="card-text">{phone.giaBan}</p>
          <button
            onClick={() => {
              viewDetail(phone);
            }}
            className="btn btn-success"
          >
            Xem chi tiết
          </button>
        </div>
      </div>
    );
  }
}
