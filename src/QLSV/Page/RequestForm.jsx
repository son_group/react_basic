import { Button, Col, DatePicker, message, Row, TimePicker } from "antd";
import moment from "moment";
import React, { Component } from "react";
import { connect } from "react-redux";
import { ADD_NEW_REQUEST } from "../constant/personconst";

class RequestForm extends Component {
  state = {
    employeeID: "",
    requestType: "Đi muộn",
    requestReason: "",
    requestDate: moment().format("YYYY-MM-DD"),
    requestTime: "",
    requestStatus: "Mới",
  };

  handleChange = (e) => {
    let tagInput = e.target;
    let { name, value } = tagInput;
    this.setState(
      {
        [name]: value,
      },
      () => {
        console.log(this.state);
      }
    );
  };

  handleDateValue = (date) => {
    this.setState(
      {
        requestDate: moment(date).format("YYYY-MM-DD"),
      },
      () => {
        console.log(this.state);
      }
    );
  };

  disabledDate = (current) => {
    // Can not select days before today and today
    return current && current.valueOf() < Date.now();
  };

  handleTime = (newTime) => {
    var time = newTime.format("HH:mm");
    this.setState(
      {
        requestTime: time,
      },
      () => {
        console.log(this.state);
      }
    );
  };

  handleCreateNewRequest = (newRequest) => {
    console.log("state", newRequest);
    this.props.addNewRequest(newRequest);
  };

  render() {
    const format = "HH:mm";
    return (
      <div className="container">
        <div className="card">
          <div className="card-header bg-dark text-white h4">
            Đăng ký yêu cầu
          </div>
          <div className="card-body">
            <form>
              <div className="row ">
                <div className="form-group col-4">
                  <label>Mã nhân viên:</label>
                  <input
                    name="employeeID"
                    type="text"
                    className="form-control"
                    value={this.state.employeeID}
                    onChange={this.handleChange}
                  />
                </div>
                <div className="form-group col-4 ">
                  <label>Loại yêu cầu:</label>

                  <select
                    name="requestType"
                    className="form-control"
                    onChange={this.handleChange}
                  >
                    <option value="Đi muộn">Đi muộn</option>
                    <option value="Về sớm">Về sớm</option>
                  </select>
                </div>
                <div className="form-group col-4">
                  <label>Lý do:</label>
                  <input
                    name="requestReason"
                    type="text"
                    className="form-control"
                    value={this.state.requestReason}
                    onChange={this.handleChange}
                  />
                </div>
              </div>
              <div className="row">
                <div className="col-4">
                  <DatePicker
                    disabledDate={this.disabledDate}
                    name="requestDate"
                    defaultValue={moment()}
                    onChange={this.handleDateValue}
                  />
                </div>
                <div className="col-4">
                  <TimePicker
                    name="requestTime"
                    format={format}
                    onChange={this.handleTime}
                  />
                </div>
                <div className="col-4 ">
                  <button
                    type="button"
                    onClick={() => {
                      this.props.addNewRequest(this.state);
                    }}
                    className="btn btn-secondary mr-4"
                  >
                    Tạo yêu cầu
                  </button>
                  <button className="btn btn-secondary">Cập nhật</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    addNewRequest: (newRequest) => {
      const action = {
        type: ADD_NEW_REQUEST,
        newRequest,
      };
      dispatch(action);
    },
  };
};

export default connect(null, mapDispatchToProps)(RequestForm);
