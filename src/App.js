import "./App.css";

import EmployeeRequest from "./QLSV/Page/EmployeeRequest";

function App() {
  return (
    <div className="App">
      <EmployeeRequest />
    </div>
  );
}

export default App;
