import { ADD_NEW_REQUEST } from "../../constant/personconst";

const initialState = {
  requestList: [
    {
      requestID: 1,
      employeeID: 3143,
      requestType: "Vào muộn",
      requestReason: "Xin nghỉ phép nửa ca",
      requestDate: "2022-10-03",
      requestTime: "11h45",
      requestStatus: "Mới",
    },
    {
      requestID: 2,
      employeeID: 3145,
      requestType: "Về sớm ",
      requestReason: "Xin nghỉ phép nửa ca",
      requestDate: "2022-10-03",
      requestTime: "12h5",
      requestStatus: "Mới",
    },
  ],
};

export const EmployeeRequestReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_NEW_REQUEST: {
      const requestList = state.requestList;
      const requestListUpdate = [...requestList, action.newRequest];
      state.requestList = requestListUpdate;
      console.log("state: ", state);

      return { ...state };
    }
    default:
      return { ...state };
  }
};
