import { Space, Table, Tag } from "antd";
import React from "react";
import { connect } from "react-redux";
import { headColumns } from "./ultil.personalrequest";

const RequestListU = () => (
  <Table
    rowKey={(record) => record.employeeID}
    columns={headColumns}
    dataSource={this.props.requestList}
  />
);

const mapStateToProps = (state) => {
  return {
    requestList: state.EmployeeRequestReducer.requestList,
  };
};

export default connect(mapStateToProps, null)(RequestListU);
